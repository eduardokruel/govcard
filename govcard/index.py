from flask import (
    Blueprint, g, redirect, render_template, request, url_for
)

from .process import process

bp = Blueprint('index', __name__)

@bp.route('/', methods=['GET', 'POST'])
def df():
    month = None
    if request.method == 'POST':
        month = request.form['month']

    if not month:
        return render_template('index.html', mostrar = False)

    df = process.get_data(month)
    total = process.get_total(df)
    maxima = process.get_max(df)
    minima = process.get_min(df)
    df_portador = process.get_portador(df)
    df_gestora = process.get_gestora(df)

    return render_template(
        'index.html',
        mostrar=True,
        total=total,
        maxima=maxima,
        minima=minima,
        df_portador=df_portador.to_html(index=True),
        df_gestora=df_gestora.to_html(index=True),
    )
